module.exports = `
  scalar Date
  scalar JSON
  enum Type {
    pre
    ico
  }

  type Query {
    transactions(size: Int, from: Int, to: Int): [Transaction]
  }
  type Transaction {
    block_hash: String,
    block_height: Int,
    block_index: Int,
    hash: String,
    addresses: [String],
    total: Float,
    fees: Float,
    size: Int,
    preference: String,
    relayed_by: String,
    gas_limit: Float,
    gas_used: Float,
    confirmed: Date,
    received: Date,
    ver: Int,
    lock_time: Int,
    double_spend: Boolean,
    vin_sz: Int,
    vout_sz: Int,
    confirmations: Int,
    confidence: Float,
    inputs: [Input],
    outputs: [Output],
    type: Type,
    currency: String
    converted: Converted
  }
  type Converted {
    fees: Float,
    total: Float
  }
  type Input {
    prev_hash: String,
    output_index: Int,
    script: String,
    output_value: Float,
    sequence: Float,
    addresses: [String],
    age: Int
  }
  type Output {
    value: Float,
    script: String,
    spent_by: String,
    addresses: [String],
    script_type: String
  }
`
