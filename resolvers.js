
const config = require('config')
const r = require('rethinkdbdash')({
  servers: [config.rethinkdb]
})
const { GraphQLScalarType } = require('graphql')
const { Kind } = require('graphql/language')
const GraphQLJSON = require('graphql-type-json')
const moment = require('moment')
module.exports = {
  JSON: GraphQLJSON,
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value) // value from the client
    },
    serialize(value) {
      const d = moment(value)
      return d.unix() // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10) // ast value is always in string format
      }
      return null
    }
  }),
  Query: {
    async transactions(obj, args, context, info) {
      const table = r.db(config.rethinkdb.db).table(config.rethinkdb.table)
      const indexes = await table.indexList().run()
      if (indexes.indexOf('received') === -1) {
        await table.indexCreate('received').run()
        await table.indexWait('received').run()
      }
      const fields = info.fieldNodes[0].selectionSet.selections.filter(s => s.kind === 'Field').map(f => f.name.value)
      if (fields.indexOf('received') === -1) {
        fields.push('received')
      }
      const from = args.from ? moment.unix(args.from) : moment('2000-01-01')
      const to = args.to ? moment.unix(args.to) : moment()
      const rows = await table.pluck(fields).run()
      return rows.filter(row => {
        return moment(row.received).unix() >= from.unix() && moment(row.received).unix() <= to.unix()
      })
    }
  }
}
